module divider(data_result,data_resultRDY,data_exception,data_operandA,data_operandB,clock,ctrl_DIV);

    input [31:0] data_operandA, data_operandB;
    input ctrl_DIV, clock;

    output [31:0] data_result;
    output data_exception, data_resultRDY;
    
    //cycle count
    wire [7:0] cycles;
    counter my_count(clock, cycles,ctrl_DIV);

    wire greater_than; 
    wire [7:0] total_iterations;
    assign total_iterations = 8'b0010_0000;
    eight_bit_comp check_cycles(data_resultRDY, greater_than,cycles,total_iterations);

    //get positive operands
    wire [31:0] negate_data_operandA, unsigned_data_operandA;
    wire A_isNotEqual, A_isLessThan, A_overflow;
    alu get_negativeA(32'b0, data_operandA, 1'b1, 1'b0, negate_data_operandA,  A_isNotEqual, A_isLessThan, A_overflow);
    mux_2 choose_signA(unsigned_data_operandA,data_operandA[31], data_operandA, negate_data_operandA);

    wire unsigned [31:0] negate_data_operandB,unsigned_data_operandB;
    wire divisor_not_eq_0, B_isLessThan, B_overflow;
    alu get_negativeB(32'b0, data_operandB, 1'b1, 1'b0, negate_data_operandB, divisor_not_eq_0, B_isLessThan, B_overflow);
    mux_2 choose_signB(unsigned_data_operandB,data_operandB[31], data_operandB, negate_data_operandB);
    assign data_exception = !divisor_not_eq_0;

    //initial register input
    wire [63:0] initial_reg_input;
    assign initial_reg_input[31:0] = unsigned_data_operandA;
    assign initial_reg_input[63:32] = 32'b0;

    wire [63:0] quotient_reg_input; 
    wire signed [63:0] quotient_after_add;
    mux_2_65 product_reg_init(quotient_reg_input, ctrl_DIV, quotient_after_add, initial_reg_input);

    // quotient register
    wire signed [63:0] current_quotient;
    wire input_enable;
    not enable(input_enable,data_resultRDY);
    register_64 quotient(current_quotient, quotient_reg_input, clock, input_enable, !divisor_not_eq_0);
    
    // positive/negative result calculation
    wire neg_result,q_isNotEqual, q_isLessThan, q_overflow;
    wire [31:0] negate_quotient;
    xor negative_result(neg_result,data_operandA[31],data_operandB[31]);
    alu get_negative_quotient(32'b0, current_quotient[31:0], 1'b1, 1'b0, negate_quotient, q_isNotEqual, q_isLessThan, q_overflow);
    mux_2 assign_result(data_result,neg_result,current_quotient[31:0],negate_quotient);

    // left shift
    wire [63:0] left_shifted;
    assign left_shifted = current_quotient <<< 1'b1;

    // add or subtract based on most significant bit
    wire signed [31:0] adder_output;
    wire add_isNotEqual,add_isLessThan,add_overflow;
    alu adder(left_shifted[63:32], unsigned_data_operandB, 1'b1, 1'b0, adder_output, add_isNotEqual,add_isLessThan,add_overflow);

   
    mux_2_65 restoring(quotient_after_add[63:32],adder_output[31],adder_output,left_shifted[63:32]);
    assign quotient_after_add[31:1]=left_shifted[31:1];

    //set least significant bit
    assign quotient_after_add[0] = !adder_output[31];

endmodule