module CLA_8(data_operandA,data_operandB,g,p,c0,s,G0,P0);
        
    input [7:0] data_operandA, data_operandB, g, p;
    input c0;

    output [7:0] s;
    output G0,P0;

    wire w0,c1;
    and and0(w0,p[0],c0);
    or carry1(c1,g[0],w0);
    xor sum0(s[0],c0,data_operandA[0],data_operandB[0]);


    wire w1a,w1b,c2;
    and and1a(w1a,p[1],p[0],c0);
    and and1b(w1b,p[1],g[0]);
    or carry2(c2,g[1],w1a,w1b);
    xor sum1(s[1],c1,data_operandA[1],data_operandB[1]);


    wire w2a,w2b,w2c,c3;
    and and2a(w2a,p[2],p[1],p[0],c0);
    and and2b(w2b,p[2],p[1],g[0]);
    and and2c(w2c,p[2],g[1]);
    or carry3(c3,g[2],w2a,w2b,w2c);
    xor sum2(s[2],c2,data_operandA[2],data_operandB[2]);


    wire w3a,w3b,w3c,w3d,c4;
    and and3a(w3a,p[3],p[2],p[1],p[0],c0);
    and and3b(w3b,p[3],p[2],p[1],g[0]);
    and and3c(w3c,p[3],p[2],g[1]);
    and and3d(w3d,p[3],g[2]);
    or carry4(c4,g[3],w3a,w3b,w3c,w3d);
    xor sum3(s[3],c3,data_operandA[3],data_operandB[3]);


    wire w4a,w4b,w4c,w4d,w4e,c5;
    and and4a(w4a,p[4],p[3],p[2],p[1],p[0],c0);
    and and4b(w4b,p[4],p[3],p[2],p[1],g[0]);
    and and4c(w4c,p[4],p[3],p[2],g[1]);
    and and4d(w4d,p[4],p[3],g[2]);
    and and4e(w4e,p[4],g[3]);
    or carry5(c5,g[4],w4a,w4b,w4c,w4d,w4e);
    xor sum4(s[4],c4,data_operandA[4],data_operandB[4]);


    wire w5a,w5b,w5c,w5d,w5e,w5f,c6;
    and and5a(w5a,p[5],p[4],p[3],p[2],p[1],p[0],c0);
    and and5b(w5b,p[5],p[4],p[3],p[2],p[1],g[0]);
    and and5c(w5c,p[5],p[4],p[3],p[2],g[1]);
    and and5d(w5d,p[5],p[4],p[3],g[2]);
    and and5e(w5e,p[5],p[4],g[3]);
    and and5f(w5f,p[5],g[4]);
    or carry6(c6,g[5],w5a,w5b,w5c,w5d,w5e,w5f);
    xor sum5(s[5],c5,data_operandA[5],data_operandB[5]);


    wire w6a,w6b,w6c,w6d,w6e,w6f,w6g,c7;
    and and6a(w6a,p[6],p[5],p[4],p[3],p[2],p[1],p[0],c0);
    and and6b(w6b,p[6],p[5],p[4],p[3],p[2],p[1],g[0]);
    and and6c(w6c,p[6],p[5],p[4],p[3],p[2],g[1]);
    and and6d(w6d,p[6],p[5],p[4],p[3],g[2]);
    and and6e(w6e,p[6],p[5],p[4],g[3]);
    and and6f(w6f,p[6],p[5],g[4]);
    and and6g(w6g,p[6],g[5]);
    or carry7(c7,g[6],w6a,w6b,w6c,w6d,w6e,w6f,w6g);
    xor sum6(s[6],c6,data_operandA[6],data_operandB[6]);

    wire w7a,w7b,w7c,w7d,w7e,w7f,w7g,w7h,c8;
    and and7a(w7a,p[7],p[6],p[5],p[4],p[3],p[2],p[1],p[0],c0);
    and and7b(w7b,p[7],p[6],p[5],p[4],p[3],p[2],p[1],g[0]);
    and and7c(w7c,p[7],p[6],p[5],p[4],p[3],p[2],g[1]);
    and and7d(w7d,p[7],p[6],p[5],p[4],p[3],g[2]);
    and and7e(w7e,p[7],p[6],p[5],p[4],g[3]);
    and and7f(w7f,p[7],p[6],p[5],g[4]);
    and and7g(w7g,p[7],p[6],g[5]);
    and and7h(w7h,p[7],g[6]);
    xor sum7(s[7],c7,data_operandA[7],data_operandB[7]);

    and P_0(P0,p[7],p[6],p[5],p[4],p[3],p[2],p[1],p[0]);
    or G_0(G0,g[7],w7a,w7b,w7c,w7d,w7e,w7f,w7g,w7h);


endmodule