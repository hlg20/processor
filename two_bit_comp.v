module two_bit_comp(EQ,GT,A,B,EQ_prev,GT_prev);

    input [1:0] A,B;
    input EQ_prev, GT_prev;

    output EQ,GT;

    wire [2:0] select;

    assign select[2:1] = A;
    assign select[0] = B[1];

    wire notB0, notGTprev;
    not not_GTprev(notGTprev,GT_prev);
    not not_B0(notB0, B[0]);

    wire zero,one;
    assign zero = 1'b0;
    assign one = 1'b1;

    wire EQmux;
    mux_8 mux_EQ(EQmux, select,notB0,zero,B[0],zero,zero,notB0,zero,B[0]);
    and EQ_out(EQ, EQmux,EQ_prev,notGTprev);

    wire GTmux, GTand;
    mux_8 mux_GT(GTmux,select,zero,zero,notB0,zero,one,zero,one,notB0);
    and GT_and(GTand,GTmux,EQ_prev,notGTprev);
    or GT_or(GT,GTand,GT_prev);

endmodule