# Processor Checkpoint for ECE 350
## Heather Grune (hlg20)

Relevant Design Choices: 

* Bypassing - use 4-input muxes to choose between register input, MX bypassing,WX bypassing, and 32'b0 (4th input is never chosen)
* Stall Logic (as seen on Lecture 13, slide 34)
    * also stall when a mult operation is taking place
* Single PW latch for multdiv (no pipelining)
* Branch Recovery (inject a nop at both F/D and D/X latches on jump/branch instructions)


