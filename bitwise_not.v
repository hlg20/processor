module bitwise_not(result, data_operandA);
        
    input [31:0] data_operandA;

    output [31:0] result;

    not not0(result[0],data_operandA[0]);
    not not1(result[1],data_operandA[1]);
    not not2(result[2],data_operandA[2]);
    not not3(result[3],data_operandA[3]);
    not not4(result[4],data_operandA[4]);
    not not5(result[5],data_operandA[5]);
    not not6(result[6],data_operandA[6]);
    not not7(result[7],data_operandA[7]);
    not not8(result[8],data_operandA[8]);
    not not9(result[9],data_operandA[9]);
    not not10(result[10],data_operandA[10]);
    not not11(result[11],data_operandA[11]);
    not not12(result[12],data_operandA[12]);
    not not13(result[13],data_operandA[13]);
    not not14(result[14],data_operandA[14]);
    not not15(result[15],data_operandA[15]);
    not not16(result[16],data_operandA[16]);
    not not17(result[17],data_operandA[17]);
    not not18(result[18],data_operandA[18]);
    not not19(result[19],data_operandA[19]);
    not not20(result[20],data_operandA[20]);
    not not21(result[21],data_operandA[21]);
    not not22(result[22],data_operandA[22]);
    not not23(result[23],data_operandA[23]);
    not not24(result[24],data_operandA[24]);
    not not25(result[25],data_operandA[25]);
    not not26(result[26],data_operandA[26]);
    not not27(result[27],data_operandA[27]);
    not not28(result[28],data_operandA[28]);
    not not29(result[29],data_operandA[29]);
    not not30(result[30],data_operandA[30]);
    not not31(result[31],data_operandA[31]);

endmodule