module mw_latch(o_out,d_out,ir_out,o_in,d_in,ir_in,clock,clr);

input [31:0] o_in,d_in,ir_in;
input clock,clr;
output [31:0] o_out,d_out,ir_out;

wire [63:0] reg_in,reg_out;
assign reg_in[63:32]= o_in;
assign reg_in[31:0] = d_in;
register_64 register(reg_out, reg_in, clock, 1'b1, clr);
assign o_out =reg_out[63:32];
assign d_out = reg_out[31:0];

register_32 ir_reg(ir_out,ir_in,clock,1'b1,clr);

endmodule