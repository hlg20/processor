module multdiv(
	data_operandA, data_operandB, 
	ctrl_MULT, ctrl_DIV, 
	clock, 
	data_result, data_exception, data_resultRDY);

    input [31:0] data_operandA, data_operandB;
    input ctrl_MULT, ctrl_DIV, clock;

    output [31:0] data_result;
    output data_exception, data_resultRDY;

    wire op_choice_enable, op_choice_out;
    or enable_operation_choice(op_choice_enable,ctrl_MULT,ctrl_DIV);
    dffe_ref mult_div(op_choice_out, ctrl_DIV, clock, op_choice_enable, 1'b0);
    wire [31:0] data_result_mult, data_result_div, ready_mult, ready_div, exception_mult, exception_div;
    booth_multiplier mult(data_result_mult,ready_mult,exception_mult,data_operandA,data_operandB,clock,ctrl_MULT);

    divider div(data_result_div,ready_div,exception_div,data_operandA,data_operandB,clock,ctrl_DIV);

    mux_2 data_choice(data_result,op_choice_out,data_result_mult,data_result_div);
    mux_2 ready_choice(data_resultRDY,op_choice_out,ready_mult,ready_div);
    mux_2 exception_choice(data_exception,op_choice_out,exception_mult,exception_div);

endmodule