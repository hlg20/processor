module counter(clock,cycles,reset);

    input clock,reset;
    output [7:0] cycles;

    wire [7:0] increment;
    wire c32;
    CLA_32 adder(cycles, 1'b1, 1'b0, increment, c32);
    register_8 cycle_count(cycles, increment, clock, 1'b1, reset);
    

endmodule