module instruction_decoder(instruction,opcode,rd,rs,rt,shamt,ALU_opcode,imm,target,rtype,itype,jtype1,jtype2);

input[31:0] instruction;
output [4:0] opcode,rd,rs,rt,shamt,ALU_opcode;
output [16:0] imm;
output [26:0] target;
output rtype,itype,jtype1,jtype2;

assign opcode = instruction[31:27];
assign rd = instruction[26:22];
assign rs = instruction[21:17];
assign rt = instruction[16:12];
assign shamt= instruction[11:7];
assign ALU_opcode = instruction[6:2];
assign imm = instruction[16:0];
assign target = instruction[26:0];

//r type
and r(rtype,!opcode[4],!opcode[3],!opcode[2],!opcode[1],!opcode[0]);
//two j-types
or j1(jtype1,opcode == 5'b00001,opcode == 5'b00011,opcode == 5'b10110,opcode == 5'b10101);
and j2(jtype2,!opcode[4],!opcode[3],opcode[2],!opcode[1],!opcode[0]); 

//i type
wire term1,term2,term3;
and i1(term1,!opcode[4],opcode[3],!opcode[2],!opcode[1],!opcode[0]);
and i2(term2,!opcode[4],!opcode[3],opcode[1],!opcode[0]);
and i3(term3,!opcode[4],!opcode[3],opcode[2],opcode[0]);
or i(itype,term1,term2,term3);

endmodule