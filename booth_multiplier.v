module booth_multiplier(data_result,data_resultRDY,data_exception,data_operandA,data_operandB,clock,ctrl_MULT);

    input [31:0] data_operandA, data_operandB;
    input ctrl_MULT, clock;
    output [31:0] data_result;
    output data_exception, data_resultRDY;

    //cycle count
    wire [7:0] cycles;
    counter my_count(clock, cycles,ctrl_MULT);

    wire greater_than; 
    wire [7:0] total_iterations;
    assign total_iterations = 8'b0010_0000;
    eight_bit_comp check_cycles(data_resultRDY, greater_than,cycles,total_iterations);
    
    //initial register input
    wire [64:0] initial_reg_input;
    assign initial_reg_input[32:1] = data_operandB;
    assign initial_reg_input[0]= 1'b0;
    assign initial_reg_input[64:33] = 32'b0;

    wire [64:0] product_reg_input;
    wire signed [64:0] after_correct_operation;
    mux_2_65 product_reg_init(product_reg_input, ctrl_MULT, after_correct_operation >>> 1'b1, initial_reg_input);
    
    // product register
    wire signed [64:0] current_product;
    wire input_enable;
    not enable(input_enable,data_resultRDY);
    register_65 product(current_product, product_reg_input, clock, input_enable, 1'b0);
    assign data_result = current_product[32:1];
    
    // control
    wire do_something;
    xor do_nothing_control(do_something, current_product[0],current_product[1]);

    //adder
    wire signed [31:0] add_output;
    wire isNotEqual,isLessThan,overflow;
    wire opcode;
    assign opcode = current_product[1];
    alu adder(current_product[64:33], data_operandA, opcode, 1'b0, add_output, isNotEqual, isLessThan, overflow);
    
    wire signed [64:0] product_after_add;
    assign product_after_add[64:33]=add_output;
    assign product_after_add[32:0]=current_product[32:0];

    
    mux_2_65 do_operation(after_correct_operation, do_something, current_product, product_after_add);


    //overflow check
    wire check_all_1, check_all_0,invalid_significant_bits, exp_sign,incorrect_sign;
    nor overflow_check1(check_all_0, current_product[33], current_product[34], current_product[35], current_product[36], 
                        current_product[37], current_product[38], current_product[39], current_product[40], current_product[41], current_product[42], 
                        current_product[43], current_product[44], current_product[45], current_product[46], current_product[47], current_product[48], 
                        current_product[49], current_product[50], current_product[51], current_product[52], current_product[53], current_product[54], 
                        current_product[55], current_product[56], current_product[57], current_product[58], current_product[59], current_product[60], 
                        current_product[61], current_product[62], current_product[63]);
    and overflow_check2(check_all_1, current_product[33], current_product[34], current_product[35], current_product[36], 
                        current_product[37], current_product[38], current_product[39], current_product[40], current_product[41], current_product[42], 
                        current_product[43], current_product[44], current_product[45], current_product[46], current_product[47], current_product[48], 
                        current_product[49], current_product[50], current_product[51], current_product[52], current_product[53], current_product[54], 
                        current_product[55], current_product[56], current_product[57], current_product[58], current_product[59], current_product[60], 
                        current_product[61], current_product[62], current_product[63]);

    nor significant_bits(invalid_significant_bits,check_all_0,check_all_1);

    wire different_sign;
    xor exp_negative(exp_sign,data_operandA[31],data_operandB[31]);
    xor sign_check(different_sign,exp_sign,data_result[31]);

    wire no_zero_operand,check_zero_operandA, check_zero_operandB,greater_than_placeholderA, greater_than_placeholderB;
    nor check_zero_A(check_zero_operandA,data_operandA[0],data_operandA[1],data_operandA[2],data_operandA[3],data_operandA[4],data_operandA[5],
                        data_operandA[6],data_operandA[7],data_operandA[8],data_operandA[9],data_operandA[10],data_operandA[11],data_operandA[12],
                        data_operandA[13],data_operandA[14],data_operandA[15],data_operandA[16],data_operandA[17],data_operandA[18],data_operandA[19],
                        data_operandA[20],data_operandA[21],data_operandA[22],data_operandA[23],data_operandA[24],data_operandA[25],data_operandA[26],
                        data_operandA[27],data_operandA[28],data_operandA[29],data_operandA[30],data_operandA[31]);
    nor check_zero_B(check_zero_operandB,data_operandB[0],data_operandB[1],data_operandB[2],data_operandB[3],data_operandB[4],data_operandB[5],
                        data_operandB[6],data_operandB[7],data_operandB[8],data_operandB[9],data_operandB[10],data_operandB[11],data_operandB[12],
                        data_operandB[13],data_operandB[14],data_operandB[15],data_operandB[16],data_operandB[17],data_operandB[18],data_operandB[19],
                        data_operandB[20],data_operandB[21],data_operandB[22],data_operandB[23],data_operandB[24],data_operandB[25],data_operandB[26],
                        data_operandB[27],data_operandB[28],data_operandB[29],data_operandB[30],data_operandB[31]);
    nor zero_op(no_zero_operand,check_zero_operandA,check_zero_operandB);
    and wrong_sign(incorrect_sign,no_zero_operand,different_sign);

    or overflow_final(data_exception,invalid_significant_bits, incorrect_sign);

endmodule