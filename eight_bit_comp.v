module eight_bit_comp(EQ,GT,A,B);
    input [7:0] A,B;
    output EQ,GT;
    
    wire zero;
    assign zero = 1'b0;
    assign one = 1'b1;
    wire EQ1,GT1,EQ2,GT2,EQ3,GT3;

    two_bit_comp comp1(EQ1,GT1,A[7:6],B[7:6],one,zero);
    two_bit_comp comp2(EQ2,GT2,A[5:4],B[5:4],EQ1,GT1);
    two_bit_comp comp3(EQ3,GT3,A[3:2],B[3:2],EQ2,GT2);
    two_bit_comp comp4(EQ,GT,A[1:0],B[1:0],EQ3,GT3);
endmodule