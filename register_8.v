module register_8(out, in, clock, input_enable, clr);

    input clock, input_enable,clr;
    input [7:0] in;
    output [7:0] out;

    dffe_ref dffe0(out[0], in[0], clock, input_enable, clr);
    dffe_ref dffe1(out[1], in[1], clock, input_enable, clr);
    dffe_ref dffe2(out[2], in[2], clock, input_enable, clr);
    dffe_ref dffe3(out[3], in[3], clock, input_enable, clr);
    dffe_ref dffe4(out[4], in[4], clock, input_enable, clr);
    dffe_ref dffe5(out[5], in[5], clock, input_enable, clr);
    dffe_ref dffe6(out[6], in[6], clock, input_enable, clr);
    dffe_ref dffe7(out[7], in[7], clock, input_enable, clr);
    
endmodule