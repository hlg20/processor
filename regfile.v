module regfile (
	clock,
	ctrl_writeEnable, ctrl_reset, ctrl_writeReg,
	ctrl_readRegA, ctrl_readRegB, data_writeReg,
	data_readRegA, data_readRegB
);

	input clock, ctrl_writeEnable, ctrl_reset;
	input [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
	input [31:0] data_writeReg;

	output [31:0] data_readRegA, data_readRegB;

	// add your code here

	wire [31:0] rd;
	decoder_32 rd_decode(rd,ctrl_writeReg);


	wire [31:0] input_enable;
	and input_en0(input_enable[0],rd[0],ctrl_writeEnable);
	and input_en1(input_enable[1],rd[1],ctrl_writeEnable);
	and input_en2(input_enable[2],rd[2],ctrl_writeEnable);
	and input_en3(input_enable[3],rd[3],ctrl_writeEnable);
	and input_en4(input_enable[4],rd[4],ctrl_writeEnable);
	and input_en5(input_enable[5],rd[5],ctrl_writeEnable);
	and input_en6(input_enable[6],rd[6],ctrl_writeEnable);
	and input_en7(input_enable[7],rd[7],ctrl_writeEnable);
	and input_en8(input_enable[8],rd[8],ctrl_writeEnable);
	and input_en9(input_enable[9],rd[9],ctrl_writeEnable);
	and input_en10(input_enable[10],rd[10],ctrl_writeEnable);
	and input_en11(input_enable[11],rd[11],ctrl_writeEnable);
	and input_en12(input_enable[12],rd[12],ctrl_writeEnable);
	and input_en13(input_enable[13],rd[13],ctrl_writeEnable);
	and input_en14(input_enable[14],rd[14],ctrl_writeEnable);
	and input_en15(input_enable[15],rd[15],ctrl_writeEnable);
	and input_en16(input_enable[16],rd[16],ctrl_writeEnable);
	and input_en17(input_enable[17],rd[17],ctrl_writeEnable);
	and input_en18(input_enable[18],rd[18],ctrl_writeEnable);
	and input_en19(input_enable[19],rd[19],ctrl_writeEnable);
	and input_en20(input_enable[20],rd[20],ctrl_writeEnable);
	and input_en21(input_enable[21],rd[21],ctrl_writeEnable);
	and input_en22(input_enable[22],rd[22],ctrl_writeEnable);
	and input_en23(input_enable[23],rd[23],ctrl_writeEnable);
	and input_en24(input_enable[24],rd[24],ctrl_writeEnable);
	and input_en25(input_enable[25],rd[25],ctrl_writeEnable);
	and input_en26(input_enable[26],rd[26],ctrl_writeEnable);
	and input_en27(input_enable[27],rd[27],ctrl_writeEnable);
	and input_en28(input_enable[28],rd[28],ctrl_writeEnable);
	and input_en29(input_enable[29],rd[29],ctrl_writeEnable);
	and input_en30(input_enable[30],rd[30],ctrl_writeEnable);
	and input_en31(input_enable[31],rd[31],ctrl_writeEnable);


	wire [31:0] w0,w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11,w12,w13,w14,w15,
				w16,w17,w18,w19,w20,w21,w22,w23,w24,w25,w26,w27,w28,w29,w30,w31;
	assign w0 = 32'b0;
	//register_32 reg0(w0, data_writeReg, clock, input_enable[0], ctrl_reset);
	register_32 reg1(w1, data_writeReg, clock, input_enable[1], ctrl_reset);
	register_32 reg2(w2, data_writeReg, clock, input_enable[2], ctrl_reset);
	register_32 reg3(w3, data_writeReg, clock, input_enable[3], ctrl_reset);
	register_32 reg4(w4, data_writeReg, clock, input_enable[4], ctrl_reset);
	register_32 reg5(w5, data_writeReg, clock, input_enable[5], ctrl_reset);
	register_32 reg6(w6, data_writeReg, clock, input_enable[6], ctrl_reset);
	register_32 reg7(w7, data_writeReg, clock, input_enable[7], ctrl_reset);
	register_32 reg8(w8, data_writeReg, clock, input_enable[8], ctrl_reset);
	register_32 reg9(w9, data_writeReg, clock, input_enable[9], ctrl_reset);
	register_32 reg10(w10, data_writeReg, clock, input_enable[10], ctrl_reset);
	register_32 reg11(w11, data_writeReg, clock, input_enable[11], ctrl_reset);
	register_32 reg12(w12, data_writeReg, clock, input_enable[12], ctrl_reset);
	register_32 reg13(w13, data_writeReg, clock, input_enable[13], ctrl_reset);
	register_32 reg14(w14, data_writeReg, clock, input_enable[14], ctrl_reset);
	register_32 reg15(w15, data_writeReg, clock, input_enable[15], ctrl_reset);
	register_32 reg16(w16, data_writeReg, clock, input_enable[16], ctrl_reset);
	register_32 reg17(w17, data_writeReg, clock, input_enable[17], ctrl_reset);
	register_32 reg18(w18, data_writeReg, clock, input_enable[18], ctrl_reset);
	register_32 reg19(w19, data_writeReg, clock, input_enable[19], ctrl_reset);
	register_32 reg20(w20, data_writeReg, clock, input_enable[20], ctrl_reset);
	register_32 reg21(w21, data_writeReg, clock, input_enable[21], ctrl_reset);
	register_32 reg22(w22, data_writeReg, clock, input_enable[22], ctrl_reset);
	register_32 reg23(w23, data_writeReg, clock, input_enable[23], ctrl_reset);
	register_32 reg24(w24, data_writeReg, clock, input_enable[24], ctrl_reset);
	register_32 reg25(w25, data_writeReg, clock, input_enable[25], ctrl_reset);
	register_32 reg26(w26, data_writeReg, clock, input_enable[26], ctrl_reset);
	register_32 reg27(w27, data_writeReg, clock, input_enable[27], ctrl_reset);
	register_32 reg28(w28, data_writeReg, clock, input_enable[28], ctrl_reset);
	register_32 reg29(w29, data_writeReg, clock, input_enable[29], ctrl_reset);
	register_32 reg30(w30, data_writeReg, clock, input_enable[30], ctrl_reset);
	register_32 reg31(w31, data_writeReg, clock, input_enable[31], ctrl_reset);


	wire [31:0] rs1, rs2;

	decoder_32 rs1_decode(rs1,ctrl_readRegA);
	tri_state_buffer rs1_buffer0(data_readRegA,w0,rs1[0]);
	tri_state_buffer rs1_buffer1(data_readRegA,w1,rs1[1]);
	tri_state_buffer rs1_buffer2(data_readRegA,w2,rs1[2]);
	tri_state_buffer rs1_buffer3(data_readRegA,w3,rs1[3]);
	tri_state_buffer rs1_buffer4(data_readRegA,w4,rs1[4]);
	tri_state_buffer rs1_buffer5(data_readRegA,w5,rs1[5]);
	tri_state_buffer rs1_buffer6(data_readRegA,w6,rs1[6]);
	tri_state_buffer rs1_buffer7(data_readRegA,w7,rs1[7]);
	tri_state_buffer rs1_buffer8(data_readRegA,w8,rs1[8]);
	tri_state_buffer rs1_buffer9(data_readRegA,w9,rs1[9]);
	tri_state_buffer rs1_buffer10(data_readRegA,w10,rs1[10]);
	tri_state_buffer rs1_buffer11(data_readRegA,w11,rs1[11]);
	tri_state_buffer rs1_buffer12(data_readRegA,w12,rs1[12]);
	tri_state_buffer rs1_buffer13(data_readRegA,w13,rs1[13]);
	tri_state_buffer rs1_buffer14(data_readRegA,w14,rs1[14]);
	tri_state_buffer rs1_buffer15(data_readRegA,w15,rs1[15]);
	tri_state_buffer rs1_buffer16(data_readRegA,w16,rs1[16]);
	tri_state_buffer rs1_buffer17(data_readRegA,w17,rs1[17]);
	tri_state_buffer rs1_buffer18(data_readRegA,w18,rs1[18]);
	tri_state_buffer rs1_buffer19(data_readRegA,w19,rs1[19]);
	tri_state_buffer rs1_buffer20(data_readRegA,w20,rs1[20]);
	tri_state_buffer rs1_buffer21(data_readRegA,w21,rs1[21]);
	tri_state_buffer rs1_buffer22(data_readRegA,w22,rs1[22]);
	tri_state_buffer rs1_buffer23(data_readRegA,w23,rs1[23]);
	tri_state_buffer rs1_buffer24(data_readRegA,w24,rs1[24]);
	tri_state_buffer rs1_buffer25(data_readRegA,w25,rs1[25]);
	tri_state_buffer rs1_buffer26(data_readRegA,w26,rs1[26]);
	tri_state_buffer rs1_buffer27(data_readRegA,w27,rs1[27]);
	tri_state_buffer rs1_buffer28(data_readRegA,w28,rs1[28]);
	tri_state_buffer rs1_buffer29(data_readRegA,w29,rs1[29]);
	tri_state_buffer rs1_buffer30(data_readRegA,w30,rs1[30]);
	tri_state_buffer rs1_buffer31(data_readRegA,w31,rs1[31]);
	
	
	decoder_32 rs2_decode(rs2,ctrl_readRegB);
	tri_state_buffer rs2_buffer0(data_readRegB,w0,rs2[0]);
	tri_state_buffer rs2_buffer1(data_readRegB,w1,rs2[1]);
	tri_state_buffer rs2_buffer2(data_readRegB,w2,rs2[2]);
	tri_state_buffer rs2_buffer3(data_readRegB,w3,rs2[3]);
	tri_state_buffer rs2_buffer4(data_readRegB,w4,rs2[4]);
	tri_state_buffer rs2_buffer5(data_readRegB,w5,rs2[5]);
	tri_state_buffer rs2_buffer6(data_readRegB,w6,rs2[6]);
	tri_state_buffer rs2_buffer7(data_readRegB,w7,rs2[7]);
	tri_state_buffer rs2_buffer8(data_readRegB,w8,rs2[8]);
	tri_state_buffer rs2_buffer9(data_readRegB,w9,rs2[9]);
	tri_state_buffer rs2_buffer10(data_readRegB,w10,rs2[10]);
	tri_state_buffer rs2_buffer11(data_readRegB,w11,rs2[11]);
	tri_state_buffer rs2_buffer12(data_readRegB,w12,rs2[12]);
	tri_state_buffer rs2_buffer13(data_readRegB,w13,rs2[13]);
	tri_state_buffer rs2_buffer14(data_readRegB,w14,rs2[14]);
	tri_state_buffer rs2_buffer15(data_readRegB,w15,rs2[15]);
	tri_state_buffer rs2_buffer16(data_readRegB,w16,rs2[16]);
	tri_state_buffer rs2_buffer17(data_readRegB,w17,rs2[17]);
	tri_state_buffer rs2_buffer18(data_readRegB,w18,rs2[18]);
	tri_state_buffer rs2_buffer19(data_readRegB,w19,rs2[19]);
	tri_state_buffer rs2_buffer20(data_readRegB,w20,rs2[20]);
	tri_state_buffer rs2_buffer21(data_readRegB,w21,rs2[21]);
	tri_state_buffer rs2_buffer22(data_readRegB,w22,rs2[22]);
	tri_state_buffer rs2_buffer23(data_readRegB,w23,rs2[23]);
	tri_state_buffer rs2_buffer24(data_readRegB,w24,rs2[24]);
	tri_state_buffer rs2_buffer25(data_readRegB,w25,rs2[25]);
	tri_state_buffer rs2_buffer26(data_readRegB,w26,rs2[26]);
	tri_state_buffer rs2_buffer27(data_readRegB,w27,rs2[27]);
	tri_state_buffer rs2_buffer28(data_readRegB,w28,rs2[28]);
	tri_state_buffer rs2_buffer29(data_readRegB,w29,rs2[29]);
	tri_state_buffer rs2_buffer30(data_readRegB,w30,rs2[30]);
	tri_state_buffer rs2_buffer31(data_readRegB,w31,rs2[31]);

endmodule
