module CLA_32(data_operandA, data_operandB, c0, sum, c32);
        
    input [31:0] data_operandA, data_operandB;
    input c0;

    output [31:0] sum;
    output c32;

    wire G0,P0,G1,P1,G2,P2,G3,P3;
    wire w8,w16a,w16b,w24a,w24b,w24c,w32a,w32b,w32c,w32d;
    wire c8,c16,c24;
    wire [31:0] p,g;

    bitwise_and gen(data_operandA,data_operandB,g);
    bitwise_or prop(data_operandA,data_operandB,p);

    CLA_8 add8(data_operandA[7:0], data_operandB[7:0], g[7:0],p[7:0],c0, sum[7:0],G0,P0);
    and and8(w8,P0,c0);
    or carry8(c8,G0,w8);

    CLA_8 add16(data_operandA[15:8], data_operandB[15:8], g[15:8],p[15:8], c8, sum[15:8], G1,P1);
    and and16a(w16a,P1,P0,c0);
    and and16b(w16b,P1,G0);
    or carry16(c16,G1,w16a,w16b);

    CLA_8 add24(data_operandA[23:16], data_operandB[23:16], g[23:16],p[23:16], c16, sum[23:16], G2,P2);
    and and24a(w24a,P2,P1,P0,c0);
    and and24b(w24b,P2,P1,G0);
    and and24c(w24c,P2,G1);
    or carry24(c24,G2,w24a,w24b,w24c);

    CLA_8 add32(data_operandA[31:24], data_operandB[31:24], g[31:24],p[31:24], c24, sum[31:24], G3,P3);
    and and32a(w32a,P3,P2,P1,P0,c0);
    and and32b(w32b,P3,P2,P1,G0);
    and and32c(w32c,P3,P2,G1);
    and and32d(w32d,P3,G2);
    or carry32(c32,G3,w32a,w32b,w32c,w32d);

endmodule