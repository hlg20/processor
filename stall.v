module stall(fd_we,pc_we,dx_mux_control,fd_opcode,fd_r1,fd_r2,dx_opcode,dx_rd,mult_operation,multdiv_at_dx);
    input [4:0] fd_r1,fd_r2,dx_rd,fd_opcode, dx_opcode;
    input mult_operation,multdiv_at_dx;
    output fd_we,pc_we,dx_mux_control;

    assign dx_mux_control = (dx_opcode == 5'b01000) && ((fd_r1 == dx_rd) || ((fd_r2 == dx_rd) && (fd_opcode != 5'b00111))) || mult_operation;
    assign fd_we = !dx_mux_control;
    assign pc_we = !dx_mux_control;

endmodule