module sign_extender(extended, imm_in);
    input [16:0] imm_in;
    output [31:0] extended;

    assign extended[16:0] = imm_in[16:0];
    assign extended[17] = imm_in[16];
    assign extended[18] = imm_in[16];
    assign extended[19] = imm_in[16];
    assign extended[20] = imm_in[16];
    assign extended[21] = imm_in[16];
    assign extended[22] = imm_in[16];
    assign extended[23] = imm_in[16];
    assign extended[24] = imm_in[16];
    assign extended[25] = imm_in[16];
    assign extended[26] = imm_in[16];
    assign extended[27] = imm_in[16];
    assign extended[28] = imm_in[16];
    assign extended[29] = imm_in[16];
    assign extended[30] = imm_in[16];
    assign extended[31] = imm_in[16];
    
endmodule