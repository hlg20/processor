module alu(data_operandA, data_operandB, ctrl_ALUopcode, ctrl_shiftamt, data_result, isNotEqual, isLessThan, overflow);
        
    input [31:0] data_operandA, data_operandB;
    input [4:0] ctrl_ALUopcode, ctrl_shiftamt;

    output [31:0] data_result;
    output isNotEqual, isLessThan, overflow;

    // add your code here:

    // add/sub (00000/00001)
    wire [31:0] not_data_operandB, b_input, adder_output;
    bitwise_not negate_b(not_data_operandB, data_operandB);
    mux_2 b_choice(b_input, ctrl_ALUopcode[0], data_operandB, not_data_operandB);
    CLA_32 adder(data_operandA, b_input, ctrl_ALUopcode[0], adder_output, c32);

    //overflow
    wire in_sign_comp, diff_out_bit;
    xnor input_bit_comp(in_sign_comp,data_operandA[31],b_input[31]);
    xor diff_sign_output(diff_out_bit, data_operandA[31],adder_output[31]);
    and overflow_check(overflow, diff_out_bit, in_sign_comp);
    
    // is not equal
    or test_not_equal(isNotEqual, adder_output[31],adder_output[30],adder_output[29], adder_output[28],
                                adder_output[27],adder_output[26],adder_output[25],adder_output[24],
                                adder_output[23],adder_output[22],adder_output[21],adder_output[20],
                                adder_output[19],adder_output[18],adder_output[17],adder_output[16],
                                adder_output[15],adder_output[14],adder_output[13],adder_output[12],
                                adder_output[11],adder_output[10],adder_output[9],adder_output[8],
                                adder_output[7],adder_output[6],adder_output[5],adder_output[4],
                                adder_output[3],adder_output[2],adder_output[1],adder_output[0]);

    // is less than
    xor test_less_than(isLessThan,adder_output[31],overflow);

    // bitwise and (00010)
    wire [31:0] and_output;
    bitwise_and bit_and(data_operandA,data_operandB,and_output);

    // bitwise or (00011)
    wire [31:0] or_output;
    bitwise_or bit_or(data_operandA,data_operandB,or_output);

    // shift left
    wire [31:0] sll_output;
    left_barrel_shift sll(sll_output, data_operandA, ctrl_shiftamt);

    // shift right
    wire [31:0] sra_output;
    right_barrel_shift sra(sra_output, data_operandA, ctrl_shiftamt);

    mux_8 select_func(data_result, ctrl_ALUopcode[2:0], adder_output,adder_output,and_output,or_output,sll_output,sra_output,data_operandA,data_operandA);

endmodule