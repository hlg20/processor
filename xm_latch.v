module xm_latch(o_out,b_out,ir_out,o_in,b_in,ir_in,clock,clr);

input [31:0] ir_in,o_in,b_in;
input clock,clr;
output [31:0] ir_out,o_out,b_out;

wire [63:0] reg_in,reg_out;
assign reg_in[63:32]= o_in;
assign reg_in[31:0] = b_in;
register_64 register(reg_out, reg_in, clock, 1'b1, clr);
assign o_out =reg_out[63:32];
assign b_out = reg_out[31:0];

register_32 ir_reg(ir_out,ir_in,clock,1'b1,clr);

endmodule