module left_barrel_shift(result, data_operandA,ctrl_shiftamt);
        
    input [31:0] data_operandA;
    input [4:0] ctrl_shiftamt;
    output [31:0] result;

    wire [31:0] out_shift_16, out_mux_16;
    shift_left_16 shift16(out_shift_16, data_operandA);
    mux_2 select_16(out_mux_16, ctrl_shiftamt[4], data_operandA, out_shift_16);

    wire [31:0] out_shift_8, out_mux_8;
    shift_left_8 shift8(out_shift_8, out_mux_16);
    mux_2 select_8(out_mux_8, ctrl_shiftamt[3], out_mux_16, out_shift_8);

    wire [31:0] out_shift_4, out_mux_4;
    shift_left_4 shift4(out_shift_4, out_mux_8);
    mux_2 select_4(out_mux_4, ctrl_shiftamt[2], out_mux_8, out_shift_4);

    wire [31:0] out_shift_2, out_mux_2;
    shift_left_2 shift2(out_shift_2, out_mux_4);
    mux_2 select_2(out_mux_2, ctrl_shiftamt[1], out_mux_4, out_shift_2);

    wire [31:0] out_shift_1;
    shift_left_1 shift1(out_shift_1, out_mux_2);
    mux_2 select_1(result, ctrl_shiftamt[0], out_mux_2, out_shift_1);

endmodule