module program_counter(pc,clock,reset,jump_address,jump,alu_output,break);

    input clock,reset,jump,break;
    input [31:0]jump_address,alu_output;
    output [31:0] pc;

    wire [31:0] incremented_pc,jump_mux,pc_mux;
    wire c32;
    CLA_32 adder(pc, 1'b1, 1'b0, incremented_pc, c32);
    mux_2 jump_choice(jump_mux, jump, incremented_pc,jump_address);
    mux_2 break_choice(pc_mux,break,jump_mux,alu_output); //this could be wrong in pipeline (correct rd value for blt has not gotten to stage 3) 
    register_64 cycle_count(pc, pc_mux, clock, 1'b1, reset);
    

endmodule