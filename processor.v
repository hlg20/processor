/**
 * READ THIS DESCRIPTION!
 *
 * This is your processor module that will contain the bulk of your code submission. You are to implement
 * a 5-stage pipelined processor in this module, accounting for hazards and implementing bypasses as
 * necessary.
 *
 * Ultimately, your processor will be tested by a master skeleton, so the
 * testbench can see which controls signal you active when. Therefore, there needs to be a way to
 * "inject" imem, dmem, and regfile interfaces from some external controller module. The skeleton
 * file, Wrapper.v, acts as a small wrapper around your processor for this purpose. Refer to Wrapper.v
 * for more details.
 *
 * As a result, this module will NOT contain the RegFile nor the memory modules. Study the inputs 
 * very carefully - the RegFile-related I/Os are merely signals to be sent to the RegFile instantiated
 * in your Wrapper module. This is the same for your memory elements. 
 *
 *
 */
module processor(
    // Control signals
    clock,                          // I: The master clock
    reset,                          // I: A reset signal

    // Imem
    address_imem,                   // O: The address of the data to get from imem
    q_imem,                         // I: The data from imem

    // Dmem
    address_dmem,                   // O: The address of the data to get or put from/to dmem
    data,                           // O: The data to write to dmem
    wren,                           // O: Write enable for dmem
    q_dmem,                         // I: The data from dmem

    // Regfile
    ctrl_writeEnable,               // O: Write enable for RegFile
    ctrl_writeReg,                  // O: Register to write to in RegFile
    ctrl_readRegA,                  // O: Register to read from port A of RegFile
    ctrl_readRegB,                  // O: Register to read from port B of RegFile
    data_writeReg,                  // O: Data to write to for RegFile
    data_readRegA,                  // I: Data from port A of RegFile
    data_readRegB                   // I: Data from port B of RegFile
	 
	);

	// Control signals
	input clock, reset;
	
	// Imem
    output [31:0] address_imem;
	input [31:0] q_imem;

	// Dmem
	output [31:0] address_dmem, data;
	output wren;
	input [31:0] q_dmem;

	// Regfile
	output ctrl_writeEnable;
	output [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
	output [31:0] data_writeReg;
	input [31:0] data_readRegA, data_readRegB;

	/* YOUR CODE STARTS HERE */

    //FETCH CYCLE
    //program counter
    wire [31:0] pc_plus_1,pc_next,pc_after_control;
    wire select_pc,c32;
    wire pc_we;
    wire isBranch;
    //assign select_pc=1'b0; //comment this out later!!
    register_32 myPC(address_imem, pc_next, !clock, pc_we, reset);
    CLA_32 adder(address_imem, 1'b1, 1'b0, pc_plus_1, c32);
    mux_2 jump_break(pc_next,isBranch,pc_plus_1,pc_after_control);
    //fd_latch
    wire [31:0] fd_ir_input;
    assign fd_ir_input = isBranch ? 32'b0 : q_imem;
    wire fd_we;
    wire [31:0] fd_pc_out,fd_ir_out;
    fd_latch fd_latch1(fd_pc_out,fd_ir_out,pc_plus_1,fd_ir_input,!clock,fd_we,reset);
    
    //decode cycle
    //outputs of mw latch
    wire [31:0] mw_o_out,mw_d_out,mw_ir_out;

    wire [4:0] opcode_fd,shamt_fd,ALU_opcode_fd,write_reg_fd,rt_fd;
    wire [16:0] imm_fd;
    wire [26:0] target_fd;
    wire rtype_fd,itype_fd,jtype1_fd,jtype2_fd;
    instruction_decoder inst_decode_fd(fd_ir_out,opcode_fd,write_reg_fd,ctrl_readRegA,rt_fd,shamt_fd,ALU_opcode_fd,imm_fd,target_fd,rtype_fd,itype_fd,jtype1_fd,jtype2_fd);
    
    wire use_rd_as_rt,use_rstatus_as_rt;
    assign use_rd_as_rt = (opcode_fd == 5'b00111) || (opcode_fd == 5'b00010) || (opcode_fd == 5'b00110) || jtype2_fd;
    assign use_rstatus_as_rt = (opcode_fd == 5'b10110);

    wire [4:0] rd_rt_choice;
    assign rd_rt_choice = use_rd_as_rt ? write_reg_fd : rt_fd;
    assign ctrl_readRegB = use_rstatus_as_rt ? 5'b11110 : rd_rt_choice;
    
    wire mult_operation,ctrl_mult,ctrl_div;
    wire dx_mux_control;
    wire [31:0] dx_ir_input1, dx_ir_input2;
    mux_2 choose_ir(dx_ir_input1,dx_mux_control,fd_ir_out,32'b0);
    mux_2 branch_ir(dx_ir_input2,isBranch,dx_ir_input1,32'b0);
    wire dx_a_reg_input,dx_b_reg_input;
    wire [31:0] dx_pc_out,dx_a_out,dx_b_out,dx_ir_out;
    dx_latch dx_latch1(dx_pc_out,dx_a_out,dx_b_out,dx_ir_out,fd_pc_out,data_readRegA,data_readRegB,dx_ir_input2,!clock, !(mult_operation || ctrl_mult || ctrl_div),reset);

    //execute cycle
    wire [31:0] alu_result;
    wire isNotEqual,isLessThan, overflow;
    
    wire [4:0] opcode_dx,shamt_dx,ALU_opcode_dx, write_reg_dx,reg_a_dx,reg_b_dx;
    wire [16:0] imm_dx;
    wire [26:0] target_dx;
    wire rtype_dx,itype_dx,jtype1_dx,jtype2_dx;
    
    wire [31:0] alu_b_input;
    wire [4:0] alu_opcode_input;
    
    instruction_decoder inst_decode_dx(dx_ir_out,opcode_dx,write_reg_dx,reg_a_dx,reg_b_dx,shamt_dx,ALU_opcode_dx,imm_dx,target_dx,rtype_dx,itype_dx,jtype1_dx,jtype2_dx);

    //stall logic
    stall stall_logic(fd_we,pc_we,dx_mux_control,opcode_fd,ctrl_readRegA,ctrl_readRegB,opcode_dx,write_reg_dx,mult_operation, ctrl_mult || ctrl_div);

    wire [31:0] reg_A_bypass,reg_B_bypass;

    wire [31:0] imm_extended;
    sign_extender extender(imm_extended,imm_dx);

    // wire use_imm;
    // assign use_imm = itype_dx;
    // assign alu_b_input = use_imm ? imm_extended : reg_B_bypass;
    // assign alu_opcode_input = use_imm ? 5'b0 : ALU_opcode_dx;
    // alu processor_alu(reg_A_bypass, alu_b_input, alu_opcode_input, shamt_dx, alu_result, isNotEqual, isLessThan, overflow);

    
    wire [1:0] use_imm;
    assign use_imm[0] = itype_dx && !(opcode_dx == 5'b00010) && !(opcode_dx == 5'b00110);
    assign use_imm[1] = ((opcode_dx == 5'b00010) || (opcode_dx == 5'b00110));
    assign alu_b_input = use_imm[0] ? imm_extended : reg_B_bypass;
    mux_4 choose_alu_opcode(alu_opcode_input,use_imm,ALU_opcode_dx,5'b0,5'b00001,ALU_opcode_dx);
    alu processor_alu(reg_A_bypass, alu_b_input, alu_opcode_input, shamt_dx, alu_result, isNotEqual, isLessThan, overflow);

    //branch/jump
    wire [31:0] branch_pc,jtype1_pc, jtype2_pc;
    CLA_32 branch_adder(dx_pc_out,imm_extended, 1'b0, branch_pc, c32);
    assign jtype1_pc = jtype1_dx ? target_dx : branch_pc;
    assign jtype2_pc = jtype2_dx ? alu_b_input : jtype1_pc;
    assign pc_after_control = jtype2_pc;
    assign isBranch = (opcode_dx == 5'b00010 && alu_result != 5'b0) || 
        (opcode_dx == 5'b00110 && !alu_result[31] && alu_result != 5'b0) || 
        (jtype1_dx && opcode_dx != 5'b10101 && opcode_dx != 5'b10110) || 
        jtype2_dx || (opcode_dx == 5'b10110 && alu_b_input!=32'b0);

    // account for overflow
    wire [31:0] xm_ir_overflow_input;
    assign xm_ir_overflow_input [31:27] = dx_ir_out[31:27];
    assign xm_ir_overflow_input [26:22] = overflow ? 5'b11110 : write_reg_dx;
    assign xm_ir_overflow_input [21:0] = dx_ir_out[21:0];
    wire [31:0] data_overflow_r30_add,data_overflow_r30_addi,data_overflow_r30_sub;
    assign data_overflow_r30_add = (overflow && ALU_opcode_dx==5'b0) ? 32'b1 : alu_result;
    assign data_overflow_r30_addi = (overflow && opcode_dx==5'b00101) ? 32'b10 : data_overflow_r30_add;
    assign data_overflow_r30_sub = (overflow && ALU_opcode_dx==5'b00001) ? 32'b11 : data_overflow_r30_addi;
    
    wire [31:0] xm_ir_setx_input,data_setx;
    assign xm_ir_setx_input [31:27] = dx_ir_out[31:27];
    assign xm_ir_setx_input [26:22] = (opcode_dx == 5'b10101)? 5'b11110 : xm_ir_overflow_input[26:22];
    assign xm_ir_setx_input [21:0] = dx_ir_out[21:0];
    assign data_setx = (opcode_dx == 5'b10101) ? target_dx : data_overflow_r30_sub;

    //assign r31 to PC+1 on jal
    wire [31:0] jal_ir_input;
    assign jal_ir_input [31:27] = dx_ir_out[31:27];
    assign jal_ir_input [26:22] = opcode_dx == 5'b00011 ? 5'b11111 : xm_ir_setx_input [26:22] ;
    assign jal_ir_input [21:0] = dx_ir_out[21:0]; 

    wire [31:0] data_in_xm_latch;
    assign data_in_xm_latch = (opcode_dx == 5'b00011) ? dx_pc_out : data_setx;
    
    //assign to latch
    wire [31:0] xm_ir_jal_choice,xm_ir_nop_choice; 
    assign xm_ir_nop_choice = mult_operation ? 32'b0 : jal_ir_input;
    wire [31:0] xm_o_out,xm_b_out,xm_ir_out;
    xm_latch xm_latch1(xm_o_out,xm_b_out,xm_ir_out,data_in_xm_latch,reg_B_bypass,xm_ir_nop_choice,!clock,reset);
    
    //multiplication
    wire data_resultRDY;
    wire [31:0] og_instruction;
    wire [31:0] mult_result;
    wire[31:0] mult_a_input, mult_b_input;
    assign ctrl_mult = (opcode_dx==5'b0) && (alu_opcode_input == 5'b00110);
    assign ctrl_div = (opcode_dx==5'b0) && (alu_opcode_input == 5'b00111);
    
    dffe_ref mult_op(mult_operation, ctrl_mult || ctrl_div, clock, ctrl_mult || ctrl_div || data_resultRDY, reset);
    register_32 ir(og_instruction,dx_ir_out,clock,ctrl_mult || ctrl_div,reset);
    
    wire [31:0] mult_register_A, mult_register_B;
    register_32 mult_regA(mult_register_A,reg_A_bypass,clock,ctrl_mult || ctrl_div,reset);
    register_32 mult_regB(mult_register_B,alu_b_input,clock,ctrl_mult || ctrl_div,reset);

    wire [31:0] mult_input_A,mult_input_B;
    assign mult_input_A = ctrl_mult || ctrl_div ? reg_A_bypass : mult_register_A;
    assign mult_input_B = ctrl_mult || ctrl_div ? alu_b_input : mult_register_B;
    multdiv multiplier(mult_input_A, mult_input_B, ctrl_mult, ctrl_div, clock, mult_result, data_exception, data_resultRDY);
    
    wire [31:0] pw_product_out, pw_ir_out;
    pw_latch product_latch1(pw_product_out, pw_ir_out, mult_result, og_instruction,!clock,data_resultRDY,ctrl_mult || ctrl_div);
    
    
    
    //Memory cycle

    wire [4:0] opcode_xm,shamt_xm,ALU_opcode_xm, write_reg_xm,reg_a_xm,reg_b_xm;
    wire [16:0] imm_xm;
    wire [26:0] target_xm;
    wire rtype_xm,itype_xm,jtype1_xm,jtype2_xm;
    
    instruction_decoder inst_decode_xm(xm_ir_out,opcode_xm,write_reg_xm,reg_a_xm,reg_b_xm,shamt_xm,ALU_opcode_xm,imm_xm,target_xm,rtype_xm,itype_xm,jtype1_xm,jtype2_xm);

    wire [31:0] xm_bypass;
    assign address_dmem = xm_o_out;
    assign data = xm_bypass;
    wire sw;
    and write_enable(sw,!xm_ir_out[31],!xm_ir_out[30],xm_ir_out[29],xm_ir_out[28],xm_ir_out[27]);
    assign wren = sw;

    mw_latch mw_latch1(mw_o_out,mw_d_out,mw_ir_out,xm_o_out,q_dmem,xm_ir_out,!clock,reset);

    //Writebackcycle
    wire [4:0] opcode_mw,shamt_mw,ALU_opcode_mw, write_reg_mw,reg_a_mw,reg_b_mw;
    wire [16:0] imm_mw;
    wire [26:0] target_mw;
    wire rtype_mw,itype_mw,jtype1_mw,jtype2_mw;
    
    instruction_decoder inst_decode_mw(mw_ir_out,opcode_mw,write_reg_mw,reg_a_mw,reg_b_mw,shamt_mw,ALU_opcode_mw,imm_mw,target_mw,rtype_mw,itype_mw,jtype1_mw,jtype2_mw);
    
    wire [4:0] opcode_pw, write_reg_pw,reg_a_pw,reg_b_pw,shamt_pw,ALU_opcode_pw;
    wire [16:0] imm_pw;
    wire [26:0] target_pw;
    wire rtype_pw,itype_pw,jtype1_pw,jtype2_pw;
    instruction_decoder inst_decode_pw(pw_ir_out,opcode_pw, write_reg_pw,reg_a_pw,reg_b_pw,shamt_pw,ALU_opcode_pw,imm_pw,target_pw,rtype_pw,itype_pw,jtype1_pw,jtype2_pw);

    wire [1:0] mw_mux_control;
    wire lw_mw;
    wire [31:0] mw_choose_data;
    and sw_calc_fd(lw_mw,!opcode_mw[4], opcode_mw[3], !opcode_mw[2],!opcode_mw[1],!opcode_mw[0]);
    assign mw_mux_control[0] = lw_mw;
    assign mw_mux_control[1] = (opcode_pw == 5'b0) && ((ALU_opcode_pw == 5'b00110) || (ALU_opcode_pw == 5'b00111));
    mux_4 store_word(mw_choose_data,mw_mux_control,mw_o_out,mw_d_out,pw_product_out,pw_product_out);

    assign ctrl_writeReg = mw_mux_control[1] ? write_reg_pw : write_reg_mw;
    assign ctrl_writeEnable = rtype_mw || (!opcode_mw[4] && !opcode_mw[3] && opcode_mw[2] && !opcode_mw[1] && opcode_mw[0]) 
        || (!opcode_mw[4] && opcode_mw[3] && !opcode_mw[2] && !opcode_mw[1] && !opcode_mw[0] || opcode_mw == 5'b10101 || opcode_mw == 5'b00011); //rtype, addi, lw, jal,sext
	assign data_writeReg = mw_choose_data;
	
    //bypassing
    wire [1:0] mux_A_control, mux_B_control;
    wire mux_C_control;

    // wire [4:0] xm_rd_bypass,mw_rd_bypass;
    wire ctrl_writeEnable_xm = rtype_xm || (!opcode_xm[4] && !opcode_xm[3] && opcode_xm[2] && !opcode_xm[1] && opcode_xm[0]) 
         || (!opcode_xm[4] && opcode_xm[3] && !opcode_xm[2] && !opcode_xm[1] && !opcode_xm[0] || opcode_xm == 5'b10101 
         || opcode_xm == 5'b00011);
    // assign xm_rd_bypass = ctrl_writeEnable_xm ? write_reg_xm : 5'b0;
    // assign mw_rd_bypass = ctrl_writeEnable ? write_reg_mw : 5'b0;
    wire [4:0] reg_a_input_bypass, reg_b_input_bypass_rtype,reg_b_input_bypass_rd, reg_b_input_bypass;
    assign reg_a_input_bypass = ctrl_writeEnable || ctrl_writeEnable_xm ? reg_a_dx : 5'b0;
    assign reg_b_input_bypass_rtype = rtype_dx ? reg_b_dx : 5'b0;
    wire use_rd_control;
    assign use_rd_control = (opcode_dx ==5'b00010 || opcode_dx == 5'b00100 || opcode_dx == 5'b00110);
    assign reg_b_input_bypass_rd = use_rd_control ? write_reg_dx : reg_b_input_bypass_rtype;
    assign reg_b_input_bypass = opcode_dx ==5'b10110 ? 5'b11110 : reg_b_input_bypass_rd;

    wire sw_mw,sw_xm;
    assign sw_mw = opcode_mw == 5'b00111;
    assign sw_xm = opcode_xm ==5'b00111;
    bypass bypass_calc(mux_A_control, mux_B_control,mux_C_control,reg_a_input_bypass,reg_b_input_bypass,reg_a_mw,write_reg_xm,ctrl_writeEnable_xm,sw_xm,write_reg_mw,ctrl_writeEnable,sw_mw);
    mux_4 bypass_regA(reg_A_bypass,mux_A_control,xm_o_out,mw_choose_data,dx_a_out,32'b0);
    mux_4 bypass_regB(reg_B_bypass,mux_B_control,xm_o_out,mw_choose_data,dx_b_out,32'b0);
    mux_2 bypass_C(xm_bypass,mux_C_control,xm_b_out,mw_choose_data);


endmodule
