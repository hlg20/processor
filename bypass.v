module bypass(alu_a_control,alu_b_control,alu_c_control,dx_r1,dx_r2,xm_rs,xm_rd,xm_we,sw_xm,mw_rd,mw_we,sw_mw);
    input [4:0] dx_r1,dx_r2,xm_rs, xm_rd,mw_rd;
    input xm_we, mw_we,sw_xm,sw_mw;
    output [1:0] alu_a_control,alu_b_control;
    output alu_c_control;

    wire xm_same_reg_A,mw_same_reg_A;
    assign xm_same_reg_A = (dx_r1 == xm_rd) && (dx_r1 != 5'b0) && (xm_we || sw_xm);
    assign mw_same_reg_A = (dx_r1 == mw_rd) && (dx_r1 != 5'b0) && (mw_we || sw_mw);

    assign alu_a_control[1] = !xm_same_reg_A && !mw_same_reg_A;
    assign alu_a_control[0] = mw_same_reg_A && !xm_same_reg_A;

    wire xm_same_reg_B,mw_same_reg_B;
    assign xm_same_reg_B = (dx_r2 == xm_rd) && (dx_r2 != 5'b0) && (xm_we || sw_xm);
    assign mw_same_reg_B = (dx_r2 == mw_rd) && (dx_r2 != 5'b0) && (mw_we|| sw_mw);

    assign alu_b_control[1] = !xm_same_reg_B && !mw_same_reg_B ;
    assign alu_b_control[0] = mw_same_reg_B && !xm_same_reg_B;

    assign alu_c_control = (xm_rd == mw_rd);

endmodule