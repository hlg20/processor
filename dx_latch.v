module dx_latch(pc_out,a_out,b_out,ir_out,pc_in,a_in,b_in,ir_in,clock,input_enable_reg,clr);

input [31:0] pc_in,a_in,b_in,ir_in;
input clock,clr,input_enable_reg;
output [31:0] pc_out,a_out,b_out,ir_out;

wire [63:0] reg_in,reg_out;
assign reg_in[63:32]= pc_in;
assign reg_in[31:0] = ir_in;
register_64 register(reg_out, reg_in, clock, 1'b1, clr);
assign pc_out = reg_out[63:32];
assign ir_out = reg_out[31:0];

wire [63:0] reg2_in,reg2_out;
assign reg2_in[63:32]= a_in;
assign reg2_in[31:0] = b_in;
register_64 register1(reg2_out, reg2_in, clock, input_enable_reg, clr);
assign a_out =reg2_out[63:32];
assign b_out = reg2_out[31:0];
endmodule