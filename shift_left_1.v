module shift_left_1(result, data_operandA);
        
    input [31:0] data_operandA;

    output [31:0] result;

    assign result[0] = 0;
    assign result[1] = data_operandA[0];
    assign result[2] = data_operandA[1];
    assign result[3] = data_operandA[2];
    assign result[4] = data_operandA[3];
    assign result[5] = data_operandA[4];
    assign result[6] = data_operandA[5];
    assign result[7] = data_operandA[6];
    assign result[8] = data_operandA[7];
    assign result[9] = data_operandA[8];
    assign result[10] = data_operandA[9];
    assign result[11] = data_operandA[10];
    assign result[12] = data_operandA[11];
    assign result[13] = data_operandA[12];
    assign result[14] = data_operandA[13];
    assign result[15] = data_operandA[14];
    assign result[16] = data_operandA[15];
    assign result[17] = data_operandA[16];
    assign result[18] = data_operandA[17];
    assign result[19] = data_operandA[18];
    assign result[20] = data_operandA[19];
    assign result[21] = data_operandA[20];
    assign result[22] = data_operandA[21];
    assign result[23] = data_operandA[22];
    assign result[24] = data_operandA[23];
    assign result[25] = data_operandA[24];
    assign result[26] = data_operandA[25];
    assign result[27] = data_operandA[26];
    assign result[28] = data_operandA[27];
    assign result[29] = data_operandA[28];
    assign result[30] = data_operandA[29];
    assign result[31] = data_operandA[30];


endmodule