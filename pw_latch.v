module pw_latch(product_out, ir_out, product_in, ir_in,clock,input_enable,clr);
    input [31:0] product_in,ir_in;
    input clock,input_enable,clr;
    output [31:0] product_out,ir_out;

    wire [63:0] reg_in,reg_out;
    assign reg_in[63:32]= product_in;
    assign reg_in[31:0] = ir_in;
    register_64 register(reg_out, reg_in, clock, input_enable, clr);
    assign product_out =reg_out[63:32];
    assign ir_out = reg_out[31:0];
endmodule