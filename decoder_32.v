module decoder_32(out,select);
    input [4:0] select;
    output [31:0] out;

    wire [31:0] one;
    assign one = 32'b0000_0000_0000_0000_0000_0000_0000_0001;
    left_barrel_shift decode_shift(out, one, select);

endmodule